# Golang application on Docker

From Go version 1.11+, Golang supports a standard toolchain for manage dependencies is Go Modules. 

Before Go modules Gophers open use dependency managers like dep or glide, but with Go Modules you don't need those 3rd-party managers.

# Get started

### Step 1: Create a simple go app main.go as follows:

```shell
package main

import (
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func main() {
	fmt.Println("Server starting at: http://localhost:8080")
	http.Handle("/", loggingMiddleware(http.HandlerFunc(handler)))
	http.ListenAndServe(":8080", nil)
}

func handler(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "package main #14")
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		log.Infof("uri: %s", req.RequestURI)
		next.ServeHTTP(w, req)
	})
}

```

### Step 2: Init Go Modules suppport


```shell
go mod init <your_init_name>
```

With me

```shell
go mod init github.com/quan-vu/go-docker-skeleton
```

### Step 3: Start your Go app

You just need to run go app or build it. Go Modules will auto get dependencies which you import in application.

```
go build
```

You can see some dependencies will be auto downloaded.

## Dockerize Go application

Now, we will create a Dockerfile to build a Docker image for our Go app.

### 1. Create a Dockerfile as follows:

	```shell
	# Go version >= v1.11 for support go mod
	FROM golang:1.12

	# Enable go mod support
	ENV GO111MODULE=on

	WORKDIR /app

	COPY . .

	RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build

	EXPOSE 8080

	# Run Go binary file of our application which named: go-docker-skeleton

	ENTRYPOINT ["/app/go-docker-skeleton"]

	# If you want to change the name of binary file when using go build run this command:
	# $ go build -o myapp main.go
	# Also change entrypoint from go-docker-skeleton -> myapp like this: ENTRYPOINT ["/app/myapp"]

	```

### 2. Build docker image:

```shell
docker build -t go-docker-skeleton_httpserver .
```

### 3. Run our docker image:

```shell
docker run -p 8080:8080 go-docker-skeleton_httpserver
```

**Result**

![](go_mod_screenshot.png)

Enjoy it!





